# Changelog

## Unreleased

### Added

- Options 'fichier' et 'extension' dans l’export CSV

## 4.2.0 - 2024-07-05

### Changed

- Comptibilité SPIP 4.*

